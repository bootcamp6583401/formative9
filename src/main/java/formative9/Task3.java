package formative9;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class Task3 {
    public static void main(String[] args) {
        Student student = new Student("Michael Scott", "23434", "Sales");

        serializeStudent(student, "student.json");

        Student deserializedStudent = deserializeStudent("student.json");

        System.out.println("Deserialized Student:");
        System.out.println(deserializedStudent);
    }

    private static void serializeStudent(Student student, String filename) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            objectMapper.writeValue(new File(filename), student);
            System.out.println("Student serialized successfully to " + filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Student deserializeStudent(String filename) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            return objectMapper.readValue(new File(filename), Student.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
