package formative9;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String jdbcUrl = "jdbc:mysql://localhost:3306/bootcamp";
        String username = "root";
        String password = "password";

        try (Connection connection = DriverManager.getConnection(jdbcUrl, username, password)) {
            if (connection != null) {
                System.out.println("Connected to the database!");

                int answer = 0;

                do {
                    System.out.println("1. Create Student");
                    System.out.println("2. Display Students");
                    System.out.println("0. Exit");

                    answer = Integer.parseInt(scanner.nextLine());

                    switch (answer) {
                        case 1:
                            System.out.print("Enter student details (name, NIM, major): ");
                            String[] studentDetails = scanner.nextLine().split(" ");
                            saveStudent(connection,
                                    new Student(studentDetails[0], studentDetails[1], studentDetails[2]));
                            break;

                        case 2:
                            displayAllStudents(connection);
                            break;
                        default:
                            break;
                    }

                } while (answer != 0);

            } else {
                System.out.println("Failed to connect to the database.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        scanner.close();
    }

    private static void saveStudent(Connection conn, Student student) {
        String sql = "INSERT INTO students (name, NIM, major) VALUES (?, ?, ?)";
        try (PreparedStatement preparedStatement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, student.getName());
            preparedStatement.setString(2, student.getNIM());
            preparedStatement.setString(3, student.getMajor());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows > 0) {
                System.out.println("Student saved successfully!");
            } else {
                System.out.println("Failed to save student.");
            }

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    int studentId = generatedKeys.getInt(1);
                    System.out.println("Student ID: " + studentId);
                }
            }
        } catch (Exception e) {

        }
    }

    private static void displayAllStudents(Connection conn) {
        String sql = "SELECT * FROM students";
        try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                System.out.println("List of Students:");
                while (resultSet.next()) {
                    System.out.println("ID: " + resultSet.getInt("id") +
                            ", Name: " + resultSet.getString("name") +
                            ", NIM: " + resultSet.getString("NIM") +
                            ", Major: " + resultSet.getString("major"));
                }
            }
        } catch (Exception e) {

        }
    }

}
