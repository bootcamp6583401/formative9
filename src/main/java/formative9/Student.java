package formative9;

import java.io.Serializable;

public class Student implements Serializable {
    private static final long serialVersionUID = 5L;

    private String name;
    private String nim;
    private String major;

    public Student() {
    }

    public Student(String name, String nim, String major) {
        this.name = name;
        this.nim = nim;
        this.major = major;
    }

    public String getName() {
        return name;
    }

    public String getNIM() {
        return nim;
    }

    public String getMajor() {
        return major;
    }

}
