package formative9;

import java.io.*;

public class Task1 {
    public static void main(String[] args) {
        Student student = new Student("Jim Halpert", "2022342342034", "Accounting");

        serializeStudent(student, "student.txt");

        Student restoredStudent = deserializeStudent("student.txt");

        if (restoredStudent != null) {
            System.out.println("Restored Student: " + restoredStudent.getName() + ", NIM: " + restoredStudent.getNIM()
                    + ", Major: " + restoredStudent.getMajor());
        }
    }

    public static void serializeStudent(Student student, String filename) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
            oos.writeObject(student);
            System.out.println("Student object serialized and saved to " + filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Student deserializeStudent(String filename) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
            Student student = (Student) ois.readObject();
            System.out.println("Student object deserialized from " + filename);
            return student;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

}
